
create table Abituriyents(
Id int primary key Identity(1,1),
Ad nvarchar(100) not null,
Soyad nvarchar(100) not null,
AtaAdi nvarchar(100) ,
IsNomresi nvarchar(20) not null,
[Status] nvarchar(50) ,
Cins nvarchar(50),
Fin nvarchar(50),
Mobil1 nvarchar(50),
Mobil2 nvarchar(50),
Mobil3 nvarchar(50),
Email nvarchar (100),
Ixtisas nvarchar(max) ,
Fakulte nvarchar(max) ,
TehsilNovu nvarchar (50),

)
create table Codes(
Id int primary key Identity(1,1),
Code nvarchar(50),
AbituriyentId int ,
foreign key (AbituriyentId) references Abituriyents (Id),
)
create table Files(
Id int primary key Identity(1,1),
AbituriyentId int,
foreign key(AbituriyentId) references Abituriyents(Id),
FileAdi nvarchar(100) ,
FilePath nvarchar(100),
)
create table Roles(
Id int primary key Identity(1,1),
Role nvarchar(30) not null,
)
create table Fakulteler(
Id int primary key Identity(1,1),
Fakulte nvarchar (max) not null,
)
create table Ixtisaslar(
Id int primary key Identity(1,1),
Ixtisas nvarchar(100) not null,
FakulteId int ,
foreign key(FakulteId) references Fakulteler(Id),
)
create table Users(
Id int primary key Identity(1,1),
Ad nvarchar(40) not null,
Soyad nvarchar(50) not null,
Parol nvarchar(max) not null,
Email nvarchar(50) not null,
MobilTelefon nvarchar(20) not null,
RoleId int not null,
Foreign key (RoleId) references Roles(Id),
Foreign key (RoleId) references Roles(Id),
FakulteId int not null,
Foreign key (FakulteId) references Fakulteler(Id),
)