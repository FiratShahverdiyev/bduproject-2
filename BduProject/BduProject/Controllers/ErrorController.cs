﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace BduProject.Controllers
{
    [Route("Error")]
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("Error");
        }
        [Route("500")]
        public IActionResult Error()
        {
            return View();
        }
    }
}
