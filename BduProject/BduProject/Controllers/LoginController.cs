﻿using BduProject.Model;
using BduProject.Repository.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BduProject.Service;

namespace BduProject.Controllers
{
    public class LoginController : Controller
    {
        private ILoginUser _loginUser;
        private ILoginAbituriyent _loginAbituriyent;

        public LoginController(ILoginUser loginUser , ILoginAbituriyent loginAbituriyent)
        {
            _loginAbituriyent = loginAbituriyent;
            _loginUser = loginUser;
        }
        [Route("login")]
        public IActionResult Index()
        {
            if (GetIP.FindIP() == "192.168.1.110")
                return View();
            else
            {
                throw new Exception();

            }
        }
        [HttpPost]
        [Route("loginUser")]
        public IActionResult LoginUser(UserLoginVm model)
        {
            if (GetIP.FindIP()== "192.168.1.110")
            {
                ClaimsIdentity identity = null;
                bool isAuthenticate = false;
                User user = _loginUser.Login(model);
                if (user != null)
                {
                    identity = new ClaimsIdentity(new[]
                     {
                    new Claim(ClaimTypes.Name,user.Name),
                    new Claim(ClaimTypes.Surname,user.Surname),
                    new Claim(ClaimTypes.Role,user.Role),
                    new Claim(ClaimTypes.Email , user.Email),
                    new Claim(ClaimTypes.MobilePhone, user.Mobil),
                    new Claim("FakulteName",user.Fakulte)
                }, "myCookie");
                    isAuthenticate = true;
                }
                if (isAuthenticate)
                {
                    var principal = new ClaimsPrincipal(identity);
                    var signIn = HttpContext.SignInAsync("myCookie", principal);
                    return RedirectToAction("Index", user.Role);
                }
            }
            else
            {
                throw new Exception();

            }
            return RedirectToAction("Index", "Login");
        }
        [HttpPost]
        [Route("loginAbituriyent")]
        public IActionResult LoginAbituriyent(AbituriyentLoginVm model)
        {
            model.Name = toUpperAze.Do(model.Name);
            model.Surname = toUpperAze.Do(model.Surname);
            ClaimsIdentity identity = null;
            bool isAuthenticate = false;
            Abituriyent abituriyent = _loginAbituriyent.Login(model);
            if (abituriyent != null)
            {
                identity = new ClaimsIdentity(new[]
                 { 
                    new Claim(ClaimTypes.Name,abituriyent.Name),
                    new Claim(ClaimTypes.Surname,abituriyent.Surname),
                    new Claim("AbiFather", abituriyent.Father),
                    new Claim(ClaimTypes.Role, "Abituriyent"),
                  
                }, "myCookie");
                isAuthenticate = true;
            }
            if (isAuthenticate)
            {
                var principal = new ClaimsPrincipal(identity);
                var signIn = HttpContext.SignInAsync("myCookie", principal);
                return RedirectToAction("Forum", "Abituriyent",abituriyent);
            }
            TempData["err"] = "Yanlis ad soyad ve ya is nomresi";
            return RedirectToAction("Index", "Abituriyent");
        }
    }
}
