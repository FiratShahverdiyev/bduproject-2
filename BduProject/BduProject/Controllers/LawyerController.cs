﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BduProject.Model;
using BduProject.Repository;
using BduProject.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BduProject.Controllers
{
    [Route("lawyer")]
    [Authorize(Roles ="Lawyer")]
    public class LawyerController : Controller
    {
        private IGetAbituriyents _getAbituriyents;
        private IGetAbituriyent _getAbituriyent;
        private IUpdateAbituriyent _updateAbituriyent;
        private IGetFile _getFile;
        public LawyerController(IGetAbituriyents getAbituriyents, IUpdateAbituriyent updateAbituriyent,
            IGetAbituriyent getAbituriyent,IGetFile getFile)
        {
            _updateAbituriyent = updateAbituriyent;
            _getAbituriyents = getAbituriyents;
            _getAbituriyent = getAbituriyent;
            _getFile = getFile;
        }
        public IActionResult Index()
        {
            return View(_getAbituriyents.GetAll());
        }
        [HttpGet]
        [Route("details")]
        public IActionResult Details(int id)
        {
            AbituriyentFile abituriyentFile = new AbituriyentFile();
            abituriyentFile.Abituriyent = _getAbituriyent.Get(id);
            abituriyentFile.Files = _getFile.GetAll(id);
            return View(abituriyentFile);
        }
        [HttpPost]
        [Route("toTyutor")]
        public IActionResult ToTyutor(int id)
        {
            _updateAbituriyent.UpdateStatus(id, "LastStep");
            return RedirectToAction("Index", "Lawyer");
        }
        [HttpPost]
        [Route("reject")]
        public IActionResult Reject(int id)
        {
            _updateAbituriyent.UpdateStatus(id, "Reject");
            return RedirectToAction("Index", "Lawyer");
        }
    }
}
