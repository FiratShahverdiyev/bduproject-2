﻿using BduProject.Model;
using BduProject.Repository;
using BduProject.Repository.Interfaces;
using BduProject.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Controllers
{
    [Route("tyutor")]
    [Authorize(Roles = "Tyutor")]
    public class TyutorController : Controller
    {
        private IGetAbituriyents _getAbituriyents;
        private IGetAbituriyent _getAbituriyent;
        private ISendMail _sendMail;
        private IGetCode _getCode;
        private IUpdateAbituriyent _updateAbituriyent;
        private  IGetFile _getFile;

        public TyutorController(IGetAbituriyents getAbituriyents,IUpdateAbituriyent updateAbituriyent,
            IGetAbituriyent getAbituriyent,ISendMail sendMail,IGetCode getCode,IGetFile getFile)
        {
            _updateAbituriyent = updateAbituriyent;
            _getAbituriyents = getAbituriyents;
            _getAbituriyent = getAbituriyent;
            _sendMail = sendMail;
            _getCode = getCode;
            _getFile = getFile;
        }
        public IActionResult Index()
        {
            return View(_getAbituriyents.GetAll());
        }
        [HttpGet]
        [Route("details")]
        public IActionResult Details(int id)
        {
            AbituriyentFile abituriyentFile = new AbituriyentFile();
            abituriyentFile.Abituriyent = _getAbituriyent.Get(id);
            abituriyentFile.Files = _getFile.GetAll(id);
            return View(abituriyentFile);
        }
        [HttpPost]
        [Route("finish")]
        public IActionResult Finish(int id)
        {
            _updateAbituriyent.UpdateStatus(id, "Completed");
            return RedirectToAction("Index", "Tyutor");

        }
        [HttpPost]
        [Route("reject")]
        public IActionResult Reject(int id)
        {
            _updateAbituriyent.UpdateStatus(id, "Rejected");
            return RedirectToAction("Index", "Tyutor");
        }
        [HttpPost]
        [Route("tolawyer")]
        public IActionResult ToLawyer(int id)
        {
            _updateAbituriyent.UpdateStatus(id, "InLawyer");
            return RedirectToAction("Index", "Tyutor");

        }
    }
}
