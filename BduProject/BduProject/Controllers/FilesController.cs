﻿using BduProject.Repository;
using BduProject.Repository.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Controllers
{
    [Authorize(Roles = "Tyutor,Lawyer")]
    [Route("files")]
    public class FilesController : Controller
    {
        private readonly IGetFile _getFile;
        public FilesController(IGetFile getFile)
        {
            _getFile = getFile;
        }
        public IActionResult Index(int id)
        {
            MyFile file = _getFile.Get(id);
            string contetnType = file.Path.Substring(file.Path.Length - 3);
            var stream = new FileStream(file.Path, FileMode.Open);
            if (contetnType == "pdf")
            {
                return new FileStreamResult(stream, "application/pdf");
            }
            if (contetnType == "peg")
            {
                return new FileStreamResult(stream, "image/jpeg");
            }
            if (contetnType == "jpg")
            {
                return new FileStreamResult(stream, "image/jpg");
            }
            throw new Exception();

        }
    }
}
