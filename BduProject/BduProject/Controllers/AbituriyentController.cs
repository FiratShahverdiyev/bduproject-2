﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BduProject.Model;
using BduProject.Repository;
using BduProject.Repository.Interfaces;
using BduProject.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace BduProject.Controllers
{
   [Route("abituriyent")]
    [Authorize(Roles = "Abituriyent")]
    public class AbituriyentController : Controller
    {
        public static bool validantion = true;
        private IUpdateAbituriyent _updateAbituriyent;
        private IGetFakulte _getFakulte;

        public AbituriyentController(IGetFakulte getFakulte,IUpdateAbituriyent updateAbituriyent)
        {
            _updateAbituriyent = updateAbituriyent;
            _getFakulte = getFakulte;
        }

        [AllowAnonymous]
      //  [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [Route("forum")]
        public IActionResult Forum(Abituriyent abituriyent)
        {
            if (!(validantion))
            ViewBag.Message = "Xais olunur formu duzgun doldurun !";
            return View(abituriyent);
        }
        [HttpPost]
        [Consumes("multipart/form-data")]
        public IActionResult SendForum([FromForm]Abituriyent abituriyent,[FromForm]Files files)
        {
            if (ModelState.IsValid)
            {
                abituriyent.Fakulte = _getFakulte.GetFakulte(abituriyent.Ixtisas);
                if (abituriyent.Fakulte == null || abituriyent.Fakulte=="")
                {
                    throw new Exception();

                }
                string dirPath = GetDirectory.DirectoryFolder(abituriyent.IsNomresi,abituriyent.Name);
                if (files.Attestat != null)
                {
                    string cont = SetContentType.SetContent(files.Attestat.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath,"Attestat"+cont);
                    fullPath = fullPath.Replace("/", @"\");
                  // using var image = Image.Load(files.Attestat.OpenReadStream());
                  //     image.Mutate(x => x.Resize(256, 256));
                 //   image.Save(fullPath);
                    CreateFolder.FileCreator(files.Attestat, abituriyent.Id, fullPath,"Attestat");
                }
                if (files.Elil12 != null)
                {

                    string cont = SetContentType.SetContent(files.Elil12.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Elil12" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Elil12, abituriyent.Id, fullPath, "Elil12");
                }
                if (files.Elillik != null)
                {

                    string cont = SetContentType.SetContent(files.Elillik.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Elillik" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Elillik, abituriyent.Id, fullPath, "Elillik");
                }
                if (files.Erize != null)
                {

                    string cont = SetContentType.SetContent(files.Erize.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Erize" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Erize, abituriyent.Id, fullPath, "Erize");
                }
                if (files.Helak != null)
                {

                    string cont = SetContentType.SetContent(files.Helak.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Helak" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Helak, abituriyent.Id, fullPath, "Helak");
                }
                if (files.Kockun != null)
                {

                    string cont = SetContentType.SetContent(files.Kockun.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Kockun" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Kockun, abituriyent.Id, fullPath, "Kockun");
                }
                if (files.MuqavileDovlet != null)
                {

                    string cont = SetContentType.SetContent(files.MuqavileDovlet.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "MuqavileDovlet" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.MuqavileDovlet, abituriyent.Id, fullPath, "MuqavileDovlet");
                }
                if (files.MuqavileOdenis != null)
                {

                    string cont = SetContentType.SetContent(files.MuqavileOdenis.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "MuqavileOdenis" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.MuqavileOdenis, abituriyent.Id, fullPath, "MuqavileOdenis");
                }
                if (files.Netice != null)
                {

                    string cont = SetContentType.SetContent(files.Netice.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Netice" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Netice, abituriyent.Id, fullPath, "Netice");
                }
                if (files.Nigah != null)
                {

                    string cont = SetContentType.SetContent(files.Nigah.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Nigah" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Nigah, abituriyent.Id, fullPath, "Nigah");
                }
                if (files.Netice != null)
                {

                    string cont = SetContentType.SetContent(files.Netice.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Netice" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Netice, abituriyent.Id, fullPath, "Netice");
                }
                if (files.Qebz != null)
                {

                    string cont = SetContentType.SetContent(files.Qebz.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Qebz" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Qebz, abituriyent.Id, fullPath, "Qebz");
                }
                if (files.Sehadetname != null)
                {

                    string cont = SetContentType.SetContent(files.Sehadetname.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Sehadetname" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Sehadetname, abituriyent.Id, fullPath, "Sehadetname");
                }
                if (files.Sehid != null)
                {

                    string cont = SetContentType.SetContent(files.Sehid.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Sehid" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Sehid, abituriyent.Id, fullPath, "Sehid");
                }
                if (files.Sekil != null)
                {

                    string cont = SetContentType.SetContent(files.Sekil.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Sekil"+cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Sekil, abituriyent.Id, fullPath, "Sekil");
                }
                if (files.Sexsiyyet != null)
                {

                    string cont = SetContentType.SetContent(files.Sexsiyyet.ContentType);
                    string fullPath = GetDirectory.FullPath(dirPath, "Sexsiyyet" + cont);
                    fullPath = fullPath.Replace("/", @"\");
                    CreateFolder.FileCreator(files.Sexsiyyet, abituriyent.Id, fullPath, "Sexsiyyet");
                }
                _updateAbituriyent.Update(abituriyent.Id, abituriyent);
                return View();
            }
            else
            {
                validantion = false;
                int id = abituriyent.Id;
                abituriyent = new Abituriyent();
                abituriyent.Id = id;
                abituriyent.Name = HttpContext.User.FindFirst(ClaimTypes.Name).Value;
                abituriyent.Surname = HttpContext.User.FindFirst(ClaimTypes.Surname).Value;
                abituriyent.Father = HttpContext.User.FindFirst("Father").Value;
                return RedirectToAction("Forum",abituriyent);
            }
        }
    }
}
