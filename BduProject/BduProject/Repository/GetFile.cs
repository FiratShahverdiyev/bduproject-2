﻿using BduProject.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{

    public class GetFile : IGetFile
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";
        public MyFile Get(int id)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "select f.Id , f.FilePath , f.FileAdi from Files f where Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    MyFile file = new MyFile();
                    file.Path = dataReader["FilePath"].ToString();
                    file.Id = Convert.ToInt32(dataReader["Id"]);
                    file.FileName = dataReader["FileAdi"].ToString();
                    dataReader.Close();
                    conn.Close();
                    return file;
                }
                dataReader.Close();
                conn.Close();
                return null;
            }
        }

        public List<MyFile> GetAll(int id)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "select f.Id , f.FilePath , f.FileAdi from Files f inner join " +
                    "Abituriyents a on a.Id=f.AbituriyentId where a.Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                List<MyFile> files = new List<MyFile>();
                while (dataReader.Read())
                {
                    MyFile file = new MyFile();
                    file.Id = Convert.ToInt32(dataReader["Id"]);
                    file.Path = dataReader["FilePath"].ToString();
                    file.FileName = dataReader["FileAdi"].ToString();
                    files.Add(file);
                }
                dataReader.Close();
                conn.Close();
                return files;
            }
        }
    }
}
