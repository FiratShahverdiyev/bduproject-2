﻿using BduProject.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class GetCode : IGetCode
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public string Get(int id)
        {
          using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "select * from Codes where Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                SqlDataReader dataReader = command.ExecuteReader();
                string code = "";
                if (dataReader.Read())
                {
                    code = dataReader["Code"].ToString();
                }
                dataReader.Close();
                conn.Close();
                return code;
            }
        }
    }
}
