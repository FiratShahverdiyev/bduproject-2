﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public interface IGetAbituriyent
    {
        public Abituriyent Get(int id);
    }
}
