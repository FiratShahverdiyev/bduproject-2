﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public interface ICreateAbituriyent
    {
        public void Create(Abituriyent abituriyent);
    }
}
