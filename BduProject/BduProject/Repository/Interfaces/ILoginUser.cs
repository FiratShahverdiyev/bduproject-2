﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository.Interfaces
{
   public interface ILoginUser
    {
        public User Login(UserLoginVm loginVm);
    }
}
