﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository.Interfaces
{
   public interface IGetCode
    {
        public string Get(int id);
    }
}
