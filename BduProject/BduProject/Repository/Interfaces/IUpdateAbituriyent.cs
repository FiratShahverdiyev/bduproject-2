﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public interface IUpdateAbituriyent
    {
        public bool Update(int id, Abituriyent abituriyent);
        public bool UpdateStatus(int id, string status);
    }
}
