﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository.Interfaces
{
    public interface IGetFile
    {
        public MyFile Get(int id);
        public List<MyFile> GetAll(int id);
    }
}
