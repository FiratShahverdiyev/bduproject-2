﻿using BduProject.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class AddFile
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public void Add(MyFile file)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string myQuery = "Insert Into Files (FilePath,AbituriyentId,FileAdi)" +
                    " values (@FilePath,@AbituriyentId,@FileName)";
                SqlCommand command = new SqlCommand(myQuery, conn);
                command.Parameters.AddWithValue("@FilePath", file.Path);
                command.Parameters.AddWithValue("@AbituriyentId", file.AbituriyentId);
                command.Parameters.AddWithValue("@FileName", file.FileName);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
