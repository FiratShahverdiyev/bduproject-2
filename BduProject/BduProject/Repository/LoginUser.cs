﻿using BduProject.Model;
using BduProject.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class LoginUser : ILoginUser
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public User Login(UserLoginVm loginVm)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string myQuery = "select * from Users u " +
                    "Inner join Roles r on u.RoleId=r.Id " +
                    "Inner join Fakulteler f on f.Id=u.FakulteId where Email=@Email and Parol=@Parol";
                SqlCommand command = new SqlCommand(myQuery, conn);
                command.Parameters.AddWithValue("@Email", loginVm.Email);
                command.Parameters.AddWithValue("@Parol", loginVm.Password);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    User user = new User();
                    user.Id =Convert.ToInt32(dataReader["Id"]);
                    user.Name = dataReader["Ad"].ToString();
                    user.Surname = dataReader["Soyad"].ToString();
                    user.Email = dataReader["Email"].ToString();
                    user.Mobil = dataReader["MobilTelefon"].ToString();
                    user.Role = dataReader["Role"].ToString();
                    user.Fakulte = dataReader["Fakulte"].ToString();
                    dataReader.Close();
                    conn.Close();
                    return user;
                }
                dataReader.Close();
                conn.Close();
                return null;
            }
        }
    }
}
