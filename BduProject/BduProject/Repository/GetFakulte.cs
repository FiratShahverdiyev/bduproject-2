﻿using BduProject.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class GetFakulte : IGetFakulte
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        string IGetFakulte.GetFakulte(string ixtisas)
        {
            if(ixtisas == null)
            {
                return null;
            }
           using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "select FakulteId from Ixtisaslar where Ixtisas=@Ixtisas";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Ixtisas", ixtisas);
                SqlDataReader dataReader = command.ExecuteReader();
                int id = 0;
                if (dataReader.Read())
                {
                    id = Convert.ToInt32(dataReader["FakulteId"]);
                }
                dataReader.Close();
                query = "select Fakulte from Fakulteler where Id=@Id";
                command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                dataReader = command.ExecuteReader();
                string fakulte = "";
                if (dataReader.Read())
                {
                    fakulte = dataReader["Fakulte"].ToString();
                }
                dataReader.Close();
                conn.Close();
                return fakulte;
            }
        }
    }
}
