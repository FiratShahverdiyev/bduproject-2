﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository.Interfaces
{
    public class LoginAbituriyent : ILoginAbituriyent
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public Abituriyent Login(AbituriyentLoginVm loginVm)
        {
            if(loginVm.Name == null || loginVm.Surname == null || loginVm.IsNomresi == null)
            {
                loginVm.Name = "";
                loginVm.Surname = "";
                loginVm.IsNomresi = "";
            }
            using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string myQuery = "select * from Abituriyents where Ad=@Ad and Soyad=@Soyad and IsNomresi=@IsNomresi";
                SqlCommand command = new SqlCommand(myQuery, conn);
                command.Parameters.AddWithValue("@Ad", loginVm.Name);
                command.Parameters.AddWithValue("@Soyad", loginVm.Surname);
                command.Parameters.AddWithValue("@IsNomresi", loginVm.IsNomresi);
                SqlDataReader dataReader = command.ExecuteReader();
                if (dataReader.Read())
                {
                    Abituriyent abituriyent = new Abituriyent();
                    abituriyent.Id = Convert.ToInt32(dataReader["Id"]);
                    abituriyent.Name = dataReader["Ad"].ToString();
                    abituriyent.Surname = dataReader["Soyad"].ToString();
                    abituriyent.Father = dataReader["AtaAdi"].ToString();
                    abituriyent.IsNomresi = dataReader["IsNomresi"].ToString();
                    abituriyent.FIN = dataReader["Fin"].ToString();
                    abituriyent.Mobil1 = dataReader["Mobil1"].ToString();
                    abituriyent.Mobil2 = dataReader["Mobil2"].ToString();
                    abituriyent.Mobil3 = dataReader["Mobil3"].ToString();
                    abituriyent.Email = dataReader["Email"].ToString();
                    abituriyent.Ixtisas = dataReader["Ixtisas"].ToString();
                    abituriyent.Fakulte = dataReader["Fakulte"].ToString();
                    abituriyent.Status = dataReader["Status"].ToString();
                    abituriyent.TehsilNovu = dataReader["TehsilNovu"].ToString();
                    abituriyent.Gender = dataReader["Cins"].ToString();
                    dataReader.Close();
                    conn.Close();
                    return abituriyent;
                }
                dataReader.Close();
                conn.Close();
                return null;
            }
        }
    }
}
