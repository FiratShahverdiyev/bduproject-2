﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class UpdateAbituriyent : IUpdateAbituriyent
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public bool Update(int id, Abituriyent abituriyent)
        {
           
            using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "update Abituriyents set Cins=@Cins , Fin=@Fin , Mobil1=@Mobil1 , Email=@Email ," +
                    " Ixtisas=@Ixtisas , Fakulte=@Fakulte , TehsilNovu=@TehsilNovu where Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                command.Parameters.AddWithValue("@Cins", abituriyent.Gender);
                command.Parameters.AddWithValue("@Fin", abituriyent.FIN);
                command.Parameters.AddWithValue("@Mobil1", abituriyent.Mobil1);
                command.Parameters.AddWithValue("@Email", abituriyent.Email);
                command.Parameters.AddWithValue("@Ixtisas", abituriyent.Ixtisas);
                command.Parameters.AddWithValue("@Fakulte", abituriyent.Fakulte);
                command.Parameters.AddWithValue("@TehsilNovu", abituriyent.TehsilNovu);
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
        }

        public bool UpdateStatus(int id, string status)
        {
            using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "update Abituriyents set Status=@Status where Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Status", status);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                conn.Close();
                return true;
            }
        }
    }
}
