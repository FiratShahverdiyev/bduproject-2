﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class DeleteAbituriyent : IDeleteAbituriyent
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";
        public void Delete(int id)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                string query = "delete from Abituriyent where Id=@Id";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Id", id);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
