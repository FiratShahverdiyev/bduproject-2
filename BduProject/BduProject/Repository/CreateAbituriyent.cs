﻿using BduProject.Model;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class CreateAbituriyent : ICreateAbituriyent
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";
        public void Create(Abituriyent abituriyent)
        {
            using(SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                string query = "insert into Abituriyents (Ad,Soyad,Father,Email,IsNomresi,Mobil1,Mobil2,Mobil3," +
                    "Ixtisas,Fakulte,Cins,Fin,TehsilNovu) values (@Email,@IsNomresi,@Mobil1,@Mobil2,@Mobil3," +
                    "@Ixtisas,@Fakulte,@Cins,@Fin,@TehsilNovu)";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.AddWithValue("@Email",abituriyent.Email);
                command.Parameters.AddWithValue("@Ad",abituriyent.Name);
                command.Parameters.AddWithValue("@Soyad",abituriyent.Surname);
                command.Parameters.AddWithValue("@Father",abituriyent.Father);
                command.Parameters.AddWithValue("@IsNomresi",abituriyent.IsNomresi);
                command.Parameters.AddWithValue("@Mobil1",abituriyent.Mobil1);
                command.Parameters.AddWithValue("@Mobil2", abituriyent.Mobil2);
                command.Parameters.AddWithValue("@Mobil3", abituriyent.Mobil3);
                command.Parameters.AddWithValue("@Ixtisas",abituriyent.Ixtisas);
                command.Parameters.AddWithValue("@Fakulte",abituriyent.Fakulte);
                command.Parameters.AddWithValue("@Cins",abituriyent.Gender);
                command.Parameters.AddWithValue("@Fin",abituriyent.FIN);
                command.Parameters.AddWithValue("@TehsilNovu",abituriyent.TehsilNovu);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }
    }
}
