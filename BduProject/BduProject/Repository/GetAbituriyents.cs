﻿using BduProject.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Repository
{
    public class GetAbituriyents : IGetAbituriyents
    {
        string connection = "Data source=DESKTOP-TKVTIK8; Initial Catalog=RegistrationAbituriyent; Integrated Security=true";

        public IEnumerable<Abituriyent> GetAll()
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                conn.Open();
                List<Abituriyent> abituriyents = new List<Abituriyent>();
                string query = "select * from Abituriyents ";
                SqlCommand command = new SqlCommand(query, conn);
                SqlDataReader dataReader = command.ExecuteReader();
                Abituriyent abituriyent = null;
                while (dataReader.Read())
                {
                    abituriyent = new Abituriyent();
                    abituriyent.Id = Convert.ToInt32(dataReader["Id"]);
                    abituriyent.Name = dataReader["Ad"].ToString();
                    abituriyent.Status = dataReader["Status"].ToString();
                    abituriyent.Surname = dataReader["Soyad"].ToString();
                    abituriyent.Fakulte = dataReader["Fakulte"].ToString();
                    abituriyent.Father = dataReader["AtaAdi"].ToString();
                    abituriyent.Email = dataReader["Email"].ToString();
                    abituriyent.FIN = dataReader["Fin"].ToString();
                    abituriyent.Mobil1 = dataReader["Mobil1"].ToString();
                    abituriyent.Mobil2 = dataReader["Mobil2"].ToString();
                    abituriyent.Mobil3 = dataReader["Mobil3"].ToString();
                    abituriyent.IsNomresi = dataReader["IsNomresi"].ToString();
                    abituriyent.Gender = dataReader["Cins"].ToString();
                    abituriyent.TehsilNovu = dataReader["TehsilNovu"].ToString();
                    abituriyents.Add(abituriyent);
                }
                dataReader.Close();
                conn.Close();
                return abituriyents;
            }
        }
    }
}
