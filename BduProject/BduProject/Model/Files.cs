﻿using BduProject.Service;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Model
{
    public class Files
    {
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        [Required]
        public IFormFile Erize { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        [Required]
        public IFormFile Sexsiyyet { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        [Required]
        public IFormFile Attestat { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        [Required]
        public IFormFile Netice { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        [Required]
        public IFormFile Sekil{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile MuqavileDovlet { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile MuqavileOdenis { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Qebz { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Kockun { get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Meskunlasma{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Sehadetname{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Sehid{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Tsek{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Elillik{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Helak{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Vefat{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Tsek32{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Nigah{ get; set; }
        [MaxFileSize(8 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" })]
        public IFormFile Elil12{ get; set; }

       
    }
}
