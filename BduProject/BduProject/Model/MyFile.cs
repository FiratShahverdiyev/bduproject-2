﻿namespace BduProject.Repository
{
    public class MyFile
    {
        public string Path { get; set; }
        public int Id { get; set; }
        public int AbituriyentId { get; set; }
        public string FileName { get; set; }
    }
}