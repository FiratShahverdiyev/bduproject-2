﻿using BduProject.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Model
{
    public class Abituriyent
    {
        public int Id { get; set; }
        [ReadOnly(true)]
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Father { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string  FIN { get; set; }
        public string  IsNomresi { get; set; }
        public string  Status { get; set; }
        [Required]
        [AllowStrings(new string[] { "female", "male" })]
        public string  Gender { get; set; }
        [Required]
        public string  Mobil1 { get; set; }
        public string  Mobil2 { get; set; }
        public string  Mobil3 { get; set; }
     //   [Required]
        [Required]
        public string  Ixtisas { get; set; }
        public string  Fakulte { get; set; }
        [Required]
        [AllowStrings(new string[] { "dovlet", "imtiyazli","odenis" })]
        public string  TehsilNovu { get; set; }

    }
}
