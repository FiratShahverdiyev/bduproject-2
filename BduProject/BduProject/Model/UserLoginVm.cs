﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Model
{
    public class UserLoginVm
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
