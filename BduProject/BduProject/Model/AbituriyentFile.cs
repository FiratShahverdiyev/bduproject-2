﻿using BduProject.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Model
{
    public class AbituriyentFile
    {
        public Abituriyent Abituriyent { get; set; }
        public List<MyFile> Files { get; set; }
    }
}
