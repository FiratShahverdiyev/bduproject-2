using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BduProject.Repository;
using BduProject.Repository.Interfaces;
using BduProject.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BduProject
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });
            services.AddSingleton<IGetAbituriyent, GetAbituriyent>();
            services.AddSingleton<IGetAbituriyents, GetAbituriyents>();
            services.AddSingleton<IUpdateAbituriyent, UpdateAbituriyent>();
            services.AddSingleton<ICreateAbituriyent, CreateAbituriyent>();
            services.AddSingleton<IDeleteAbituriyent, DeleteAbituriyent>();
            services.AddSingleton<ILoginAbituriyent, LoginAbituriyent>();
            services.AddSingleton<ILoginUser, LoginUser>();
            services.AddSingleton<ISendMail, SendMail>();
            services.AddSingleton<IGetCode, GetCode>();
            services.AddSingleton<IGetFile, GetFile>();
            services.AddSingleton<IGetFakulte, GetFakulte>();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication("myCookie").AddCookie("myCookie", config => {
                config.Cookie.Name = "Web.Cookie";
            });
            services.AddControllersWithViews();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
