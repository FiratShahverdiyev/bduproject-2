﻿namespace BduProject.Service
{
    public interface ISendMail
    {
        public void Send(string toEmail, string receiverName, string myEmail, string name, string password, string codeMessage);
    }
}