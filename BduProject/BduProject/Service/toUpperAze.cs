﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class toUpperAze
    {
        public static string Do(string str)
        {
            if (str!=null && str.Length > 0)
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < str.Length; i++)
                {
                    char c = str[i];
                    switch (c)
                    {
                        case 'a':
                            c = 'A';
                            break;
                        case 'b':
                            c = 'B';
                            break;
                        case 'c':
                            c = 'C';
                            break;
                        case 'd':
                            c = 'D';
                            break;
                        case 'q':
                            c = 'Q';
                            break;
                        case 'e':
                            c = 'E';
                            break;
                        case 'r':
                            c = 'R';
                            break;
                        case 't':
                            c = 'T';
                            break;
                        case 'y':
                            c = 'Y';
                            break;
                        case 'u':
                            c = 'U';
                            break;
                        case 'i':
                            c = 'İ';
                            break;
                        case 'o':
                            c = 'O';
                            break;
                        case 'p':
                            c = 'P';
                            break;
                        case 'ö':
                            c = 'Ö';
                            break;
                        case 'ğ':
                            c = 'Ğ';
                            break;
                        case 's':
                            c = 'S';
                            break;
                        case 'f':
                            c = 'F';
                            break;
                        case 'g':
                            c = 'G';
                            break;
                        case 'h':
                            c = 'H';
                            break;
                        case 'j':
                            c = 'J';
                            break;
                        case 'k':
                            c = 'K';
                            break;
                        case 'l':
                            c = 'L';
                            break;
                        case 'ı':
                            c = 'I';
                            break;
                        case 'ə':
                            c = 'Ə';
                            break;
                        case 'z':
                            c = 'Z';
                            break;
                        case 'x':
                            c = 'X';
                            break;
                        case 'v':
                            c = 'V';
                            break;
                        case 'n':
                            c = 'N';
                            break;
                        case 'm':
                            c = 'M';
                            break;
                        case 'ç':
                            c = 'Ç';
                            break;
                        case 'ş':
                            c = 'Ş';
                            break;
                            //32  ? ? ? 
                    }
                    builder.Append(c);
                }
                return builder.ToString();
            }
            return "";
        }
    }
}
