﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class SendMail : ISendMail
    {
        public void Send(string toEmail, string receiverName, string myEmail, string name, string password , string codeMessage)
        {
            using (var message = new MailMessage())
            {
                message.To.Add(new MailAddress(toEmail, receiverName));
                message.From = new MailAddress(myEmail, name);
                message.Subject = "privacy code";
                message.Body = codeMessage;
                message.IsBodyHtml = true;
                using (var client = new System.Net.Mail.SmtpClient("smtp.mail.ru"))
                {
                    client.Port = 587;
                    client.Credentials = new NetworkCredential(myEmail, password);
                    client.EnableSsl = true;
                    client.Send(message);
                    client.Dispose();
                }
            }
        }
    }
}
