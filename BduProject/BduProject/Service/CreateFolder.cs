﻿using BduProject.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class CreateFolder
    {
        public static void FileCreator(IFormFile file,int abituriyentId,string fullpath,string fileName)
        {
            AddFile addFile = new AddFile();
          
                using (FileStream fs = System.IO.File.Create(fullpath))
                {
                    //   Helper.Resize((Image)file, size).CopyTo(fs);
                    file.CopyTo(fs);
                    fs.Flush();
                }
                MyFile myFile = new MyFile();
                myFile.Path = fullpath;
                myFile.AbituriyentId = abituriyentId;
                myFile.FileName = fileName;
            addFile.Add(myFile);
            
        }
    }
}
