﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class SetContentType
    {
        public static string SetContent(string cont)
        {
            
            if (cont[cont.Length - 5] == '/')
            {
                cont = ".jpeg";
            }
            else
            {
                if (cont[cont.Length - 3] == 'j')
                {
                    cont = ".jpg";
                }
                else if (cont[cont.Length - 3] == 'p')
                {
                    cont = ".pdf";
                }
                else
                {
                    //Olmaz
                }
            }
            return cont;
        }
    }
}
