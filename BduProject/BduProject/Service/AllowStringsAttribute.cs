﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class AllowStringsAttribute : ValidationAttribute
    {
     
            private readonly string[] _genders;
            public AllowStringsAttribute(string[] genders)
            {
            _genders = genders;
            }

            protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
            {
            if (value == null)
            {
                return new ValidationResult(GetErrorMessage());
            }
            var gender = value.ToString();
                if (gender != null)
                {
                    if (!_genders.Contains(gender.ToLower()))
                    {
                        return new ValidationResult(GetErrorMessage());
                    }
                return ValidationResult.Success;
                }

                return new ValidationResult(GetErrorMessage());
            }

            public string GetErrorMessage()
            {
                return $"This gender is not allowed!";
            }
        }
    
}
