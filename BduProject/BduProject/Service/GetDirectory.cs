﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BduProject.Service
{
    public class GetDirectory
    {
        public static string DirectoryFolder(string isnomresi,string name)
        {
            string dirPath = Path.Combine(@"C:\Users\HP\Desktop\Abituriyentler", isnomresi+"_"+name);
            Directory.CreateDirectory(dirPath);
            return dirPath;
        }
        public static string FullPath(string dirPath,string fileName)
        {
            return Path.Combine(dirPath, fileName);
        }
    }
}
